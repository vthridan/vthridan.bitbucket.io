var files_dup =
[
    [ "bno055.py", "bno055_8py.html", "bno055_8py" ],
    [ "bno055_base.py", "bno055__base_8py.html", "bno055__base_8py" ],
    [ "busy_task.py", "busy__task_8py.html", [
      [ "BusyTask", "classbusy__task_1_1BusyTask.html", "classbusy__task_1_1BusyTask" ]
    ] ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Lab0x01.py", "Lab0x01_8py.html", "Lab0x01_8py" ],
    [ "Lab0x04.py", "Lab0x04_8py.html", "Lab0x04_8py" ],
    [ "Lab2_Try2.py", "Lab2__Try2_8py.html", "Lab2__Try2_8py" ],
    [ "Lab3main.py", "Lab3main_8py.html", "Lab3main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "motor_driver.py", "motor__driver_8py.html", "motor__driver_8py" ],
    [ "print_task.py", "print__task_8py.html", "print__task_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "Touch_Driver.py", "Touch__Driver_8py.html", "Touch__Driver_8py" ],
    [ "touch_panel.py", "touch__panel_8py.html", "touch__panel_8py" ],
    [ "UI_lab3_tests.py", "UI__lab3__tests_8py.html", "UI__lab3__tests_8py" ]
];