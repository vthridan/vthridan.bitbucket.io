var Lab2__Try2_8py =
[
    [ "IC_CB", "Lab2__Try2_8py.html#a6794e76d4a07588df8c35dccefa0c723", null ],
    [ "OC_CB", "Lab2__Try2_8py.html#ac67a15556181807ebea9e059ddf4bd64", null ],
    [ "A5", "Lab2__Try2_8py.html#ae83dc1de5789a61ead7b659992fea7f3", null ],
    [ "app_var", "Lab2__Try2_8py.html#acf97507f2a3526c2c01cdb99a86c81be", null ],
    [ "aresponse", "Lab2__Try2_8py.html#a072b3375a5e87938fbc36d8d9e188c8c", null ],
    [ "delay_var", "Lab2__Try2_8py.html#a16ea6d615e34ee3c3aac7059e78c8e89", null ],
    [ "extint", "Lab2__Try2_8py.html#aad79c72a12b6f123f9748c1ca6bf63f7", null ],
    [ "last_cb_time", "Lab2__Try2_8py.html#a5533a563776e9ad3f5c6fbc6a094de72", null ],
    [ "lastCompare", "Lab2__Try2_8py.html#a99de39f74439f9347c9fe9d1cc86af0f", null ],
    [ "mode", "Lab2__Try2_8py.html#ae5cdb13ab5aad318fc8d011d27d5d6d2", null ],
    [ "pin_var", "Lab2__Try2_8py.html#a2ec01a295e8fee75ac450d0fdb872d13", null ],
    [ "pinA5", "Lab2__Try2_8py.html#aa5bc11612337088e82b0c20e77261fa2", null ],
    [ "pinB3", "Lab2__Try2_8py.html#a70365752d4f8bf22ed1c35ba81c8f76f", null ],
    [ "t_pushed", "Lab2__Try2_8py.html#a9045186c0386dd92e6343d7b4a1ada3a", null ],
    [ "tim2", "Lab2__Try2_8py.html#aba0d81898adb3ba103d7d92b8c84fa90", null ],
    [ "tim2ch1", "Lab2__Try2_8py.html#a07bacc07aa9b901c6c255a9205b9c719", null ],
    [ "tim2ch2", "Lab2__Try2_8py.html#a23b9aeebafd57e1d9aca3b06c84ac879", null ],
    [ "variance", "Lab2__Try2_8py.html#a1fff252a3942b9cbf7f5a03c0f8b055b", null ]
];