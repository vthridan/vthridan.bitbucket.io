var Lab0x04_8py =
[
    [ "adc", "Lab0x04_8py.html#a00353295d300e3803319c111fcfbc2e2", null ],
    [ "cycle", "Lab0x04_8py.html#a7b0db584c75be3bdfa68e83f3b78f224", null ],
    [ "data", "Lab0x04_8py.html#aff5efe46a31b5efa58d9f1ec778a2d13", null ],
    [ "filename", "Lab0x04_8py.html#a8f07f520e2e59599c59b8aa6f718c95f", null ],
    [ "header", "Lab0x04_8py.html#ad3c795dc25e8faf721e87d440c33b640", null ],
    [ "hrs", "Lab0x04_8py.html#a67ed9bc14b7ade2c74bc5b1d7b604884", null ],
    [ "MCP9808", "Lab0x04_8py.html#a99fd0e2c6c3defc7c913f671858fbaad", null ],
    [ "t0", "Lab0x04_8py.html#af974c97353d0ccdbd3b690c11c1ebd2c", null ],
    [ "t_current", "Lab0x04_8py.html#a85894d457193c3edc6bf83a18b8dbfb1", null ],
    [ "t_iter", "Lab0x04_8py.html#a500848ef9796f27a5edaba67689e9e5e", null ],
    [ "temp", "Lab0x04_8py.html#a71b6249ccded52c49ada6e93f7dd7c71", null ],
    [ "temp_am", "Lab0x04_8py.html#a58f2b46699f4691678ab2de8b4c96dae", null ],
    [ "ticks_0", "Lab0x04_8py.html#a46d062adb844d660c5365c8c39fab2c7", null ],
    [ "time_ms_run", "Lab0x04_8py.html#a4d0d53526bb35acd409f2adc0153f1c4", null ],
    [ "time_s", "Lab0x04_8py.html#a25fb27f22f0d2003015f214c6db3760a", null ],
    [ "total_time", "Lab0x04_8py.html#a570bb1f24037c12bb09c8cccc60bafcb", null ],
    [ "volts", "Lab0x04_8py.html#afd88e3f0a66e85b394cc4cd2c7b23238", null ]
];