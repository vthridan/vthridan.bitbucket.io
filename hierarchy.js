var hierarchy =
[
    [ "bno055_base.BNO055_BASE", "classbno055__base_1_1BNO055__BASE.html", [
      [ "bno055.BNO055", "classbno055_1_1BNO055.html", null ]
    ] ],
    [ "busy_task.BusyTask", "classbusy__task_1_1BusyTask.html", null ],
    [ "Encoder.Encoder", "classEncoder_1_1Encoder.html", null ],
    [ "mcp9808.MCP9808", "classmcp9808_1_1MCP9808.html", null ],
    [ "motor_driver.MotorDriver", "classmotor__driver_1_1MotorDriver.html", null ],
    [ "task_share.Queue", "classtask__share_1_1Queue.html", null ],
    [ "task_share.Share", "classtask__share_1_1Share.html", null ],
    [ "cotask.Task", "classcotask_1_1Task.html", null ],
    [ "cotask.TaskList", "classcotask_1_1TaskList.html", null ],
    [ "Touch_Driver.Touch_Driver", "classTouch__Driver_1_1Touch__Driver.html", null ]
];